# Import packages needed for the scenario
import mosaik
import numpy as np


def main():
    # Simulation config
    sim_config = {
        "MidasStore": {"python": "midas.modules.store.simulator:MidasHdf5"},
        "WeatherData": {
            "python": "midas.modules.weather.simulator:WeatherDataSimulator"
        },
        "Pysimmods": {
            "cmd": "%(python)s -m pysimmods.mosaik.pysim_mosaik %(addr)s"
        },
    }

    # Other configuration
    start_date = "2020-01-01 00:00:00+0100"
    end = 24 * 60 * 60  # One day
    step_size = 15 * 60  # 15 minutes
    data_path = "/home/sbalduin/.config/midas/midas_data"
    output_path = "./"

    # Setup the world
    world = mosaik.World(sim_config)

    # Start all simulators
    store_sim = world.start("MidasStore", step_size=step_size)
    weather_sim = world.start(
        "WeatherData",
        step_size=step_size,
        start_date=start_date,
        data_path=data_path,
        filename="WeatherBre2009-2020.hdf5",
        noise_factor=0.05,
    )
    der_sim = world.start(
        "Pysimmods", step_size=step_size, start_date=start_date
    )

    # Start all models
    database = store_sim.Database(
        filename=output_path + "weather_and_der.hdf5"
    )
    weather = weather_sim.WeatherCurrent(
        interpolate=True, randomize=True, seed=1234
    )
    pv_0 = der_sim.Photovoltaic(
        params={
            "pv": {
                "a_m2": 8000.0,
                "eta_percent": 25.0,
                "is_static_t_module": False,
                "sign_convention": "active",
            },
            "inverter": {
                "sn_kva": 2222.222222222222,
                "q_control": "prioritize_p",
                "cos_phi": 0.9,
                "inverter_mode": "inductive",
                "sign_convention": "active",
            },
            "sign_convention": "active",
            "sn_mva": 2.2222222222222223,
        },
        inits={"pv": {"t_module_deg_celsius": 5.0}, "inverter": {}},
    )
    hvac_0 = der_sim.HVAC(
        params={
            "p_max_kw": 1279,
            "eta_percent": 45.0,
            "length": 100.0,
            "width": 60.0,
            "height": 12.0,
            "d_m": 0.2,
            "lambda_w_per_m_k": 0.15,
            "t_min_deg_celsius": -30.0,
            "t_max_deg_celsius": -18.0,
            "thaw_factor": 50.0,
            "cool_factor": 0.011,
        },
        inits={
            "mass_kg": 448000.0,
            "c_j_per_kg_k": 3850.0,
            "theta_t_deg_celsius": -20,
            "cooling": True,
            "mode": "auto",
        },
    )
    chp_0 = der_sim.CHP(
        params={
            "chp": {
                "p_max_kw": 400,
                "p_min_kw": 200,
                "p_2_p_th_percent": 128.0,
                "eta_max_percent": 88.7,
                "eta_min_percent": 87.4,
                "own_consumption_kw": 5.3,
                "active_min_s": 0.0,
                "inactive_min_s": 0.0,
                "lubricant_max_l": 155.0,
                "lubricant_ml_per_h": 130.0,
                "storage_cap_l": 60000.0,
                "storage_consumption_kwh_per_day": 2.13,
                "storage_t_min_c": 40.0,
                "storage_t_max_c": 85.0,
                "sign_convention": "active",
            },
            "sign_convention": "active",
            "flip_e_th_demand_sign": True,
            "household": {"chp_p_th_prod_kw": 256.0},
        },
        inits={
            "chp": {
                "lubricant_l": 154,
                "active_s": 1800,
                "inactive_s": 0,
                "is_active": True,
                "storage_t_c": 50,
            },
            "household": {
                "start_date": "2017-01-01 00:00:00+0100",
                "t_last_3": 2.1,
                "t_last_2": 1.7,
                "t_last_1": 4.2,
            },
        },
    )
    bat_0 = der_sim.Battery(
        params={
            "cap_kwh": 2500.0,
            "soc_min_percent": 15,
            "p_charge_max_kw": 500.0,
            "p_discharge_max_kw": 500.0,
            "eta_pc": [-2.109566, 0.403556, 97.11077],
        },
        inits={"soc_percent": 50.0},
    )

    # Connect entities
    world.connect(
        weather,
        database,
        "t_air_deg_celsius",
        "day_avg_t_air_deg_celsius",
        "bh_w_per_m2",
        "dh_w_per_m2",
        "wind_v_m_per_s",
        "wind_direction_deg",
    )

    world.connect(
        weather, pv_0, "bh_w_per_m2", "dh_w_per_m2", "t_air_deg_celsius"
    )
    world.connect(weather, hvac_0, "t_air_deg_celsius")
    world.connect(weather, chp_0, "day_avg_t_air_deg_celsius")

    world.connect(pv_0, database, "p_mw", "q_mvar", "t_module_deg_celsius")
    world.connect(hvac_0, database, "p_mw", "q_mvar")
    world.connect(
        chp_0, database, "p_mw", "q_mvar", "p_th_mw", "e_th_demand_kwh"
    )
    world.connect(bat_0, database, "p_mw", "q_mvar", "soc_percent")

    world.run(until=end)


if __name__ == "__main__":
    main()
