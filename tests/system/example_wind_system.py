import os
from datetime import datetime, timedelta, timezone

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

from pysimmods.generator import WindPowerPlantSystem
from pysimmods.generator.windsystemsim.presets import wind_presets

T_AIR = "WeatherCurrent__0___t_air_deg_celsius"
WIND = "WeatherCurrent__0___wind_v_m_per_s"
WIND_DIR = "WeatherCurrent__0___wind_dir_deg"
PRESSURE = "WeatherCurrent__0___air_pressure_hpa"
WD_PATH = os.path.abspath(
    os.path.join(__file__, "..", "..", "fixtures", "weather-time-series.csv")
)


def main():
    wd = pd.read_csv(WD_PATH, index_col=0)
    wind_sys = WindPowerPlantSystem(*wind_presets(pn_max_kw=8))
    now_dt = datetime(2021, 1, 1, 10, 0, 0, tzinfo=timezone.utc)

    steps = 96
    step_size = 900
    index = np.arange(steps)
    p_kws = np.zeros(steps)
    q_kvars = np.zeros(steps)
    cos_phis = np.zeros(steps)
    winds = np.zeros(steps)

    for i in range(steps):
        widx = datetime_to_index(now_dt)
        wind_sys.set_step_size(step_size)
        wind_sys.set_now_dt(now_dt)
        wind_sys.inputs.t_air_deg_celsius = wd.iloc[widx][T_AIR]
        wind_sys.inputs.wind_v_m_per_s = wd.iloc[widx][WIND]
        wind_sys.inputs.air_pressure_hpa = wd.iloc[widx][PRESSURE]

        wind_sys.step()

        p_kws[i] = wind_sys.get_p_kw()
        q_kvars[i] = wind_sys.get_q_kvar()
        cos_phis[i] = wind_sys.get_cos_phi()
        winds[i] = wd.iloc[widx][WIND]
        now_dt += timedelta(seconds=step_size)

    # Plot the results
    plot_results(index, p_kws, q_kvars, cos_phis, winds, "wind_sys.png")


def datetime_to_index(
    dt: datetime, step_size: int = 900, steps_per_year: int = 35135
) -> int:
    dif = dt - dt.replace(month=1, day=1, hour=0, minute=0, second=0)
    dif = dif.total_seconds()
    idx = int(dif // step_size) % steps_per_year

    return idx


def plot_results(index, p_kws, q_kvars, cos_phis, winds, filename):
    _, axes = plt.subplots(2, 2, figsize=(12, 6))

    axes[0][0].bar(index, p_kws, color="green")
    axes[0][0].set_ylabel("active power [kW]")
    axes[1][0].bar(index, q_kvars, color="blue")
    axes[1][0].set_ylabel("reactive power [kVAr]")

    axes[0][1].plot(cos_phis, color="black")
    axes[0][1].set_ylabel("cos phi")
    axes[1][1].plot(winds, color="orange")
    axes[1][1].set_ylabel("wind speed [m/s]")

    plt.savefig(filename, dpi=300, bbox_inches="tight")
    plt.close()


if __name__ == "__main__":
    main()
