import unittest

import numpy as np

from pysimmods.util.wind_at_height import hellman, logarithmic_profile


class TestWindAtHeight(unittest.TestCase):
    def setUp(self):
        self.wind_speed = 7.0175
        self.wind_speed_height = 10
        self.target_height = 150

    def test_compare(self):
        w_logarithmic = logarithmic_profile(
            self.wind_speed, self.wind_speed_height, self.target_height
        )
        w_hellmann = hellman(
            self.wind_speed, self.wind_speed_height, self.target_height
        )

        self.assertNotEqual(w_logarithmic, w_hellmann)
        self.assertAlmostEqual(11.5425, w_logarithmic, places=4)
        self.assertAlmostEqual(10.3323, w_hellmann, places=4)


if __name__ == "__main__":
    unittest.main()
