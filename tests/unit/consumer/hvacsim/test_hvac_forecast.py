"""This module contains tests for the biogas plant in combination
with the flexibility model.
"""

import unittest
from datetime import datetime, timedelta

import numpy as np
import pandas as pd

from pysimmods.consumer.hvacsim import HVAC
from pysimmods.consumer.hvacsim.presets import (
    hvac_1279kw_init,
    hvac_1279kw_params,
)
from pysimmods.other.flexibility.forecast_model import ForecastModel
from pysimmods.util.date_util import GER


class TestHVACForecast(unittest.TestCase):
    def setUp(self):
        self.start_date = "2017-01-01 00:00:00+0100"
        self.forecasts = pd.DataFrame(
            data=np.ones(15) * 15,
            columns=["t_air_deg_celsius"],
            index=pd.date_range(
                datetime.strptime(self.start_date, GER),
                periods=15,
                freq="15min",
            ),
        )

    def test_step(self):
        # model = HVAC(hvac_1279kw_params(), hvac_1279kw_init())
        fcm = ForecastModel(HVAC(hvac_1279kw_params(), hvac_1279kw_init()))
        fcm.update_forecasts(self.forecasts)

        fcm.set_now_dt(datetime.strptime(self.start_date, GER))
        fcm.set_step_size(15 * 60)
        fcm.set_p_kw(1279.0)
        fcm.inputs.t_air_deg_celsius = 28

        fcm.step()

        self.assertEqual(fcm.state.p_kw, 1279.0)
        self.assertEqual(fcm.state.q_kvar, 0)

    def test_schedule(self):
        """This tests fails when forecast horizon is reduced from *2 to *1.
        Apparently, something related to get_state set_state does not work
        correctly.
        """
        fcm = ForecastModel(HVAC(hvac_1279kw_params(), hvac_1279kw_init()))
        now_dt = datetime.strptime(self.start_date, GER)

        expected = [
            0,
            127.9,
            255.8,
            383.7,
            511.6,
            639.5,
            767.4,
            895.3,
            1023.2,
            1151.1,
            1279.0,
        ]

        schedule_df = pd.DataFrame(
            data=np.arange(0, 1406.9, step=127.9),
            columns=["p_set_kw"],
            index=pd.date_range(now_dt, freq="15min", periods=11),
        )
        fcm.set_now_dt(now_dt)
        fcm.set_step_size(900)
        fcm.update_forecasts(self.forecasts)
        fcm.update_schedule(schedule_df)
        for setpoint in expected:
            fcm.set_now_dt(now_dt)
            fcm.set_step_size(15 * 60)
            fcm.inputs.t_air_deg_celsius = 28
            fcm.step()
            now_dt += timedelta(seconds=900)
            self.assertAlmostEqual(setpoint, fcm.get_p_kw())


if __name__ == "__main__":
    unittest.main()
