"""Testcase for the flex model used with the battery model."""

import unittest
from datetime import datetime

import numpy as np
import pandas as pd

from pysimmods.consumer.hvacsim import HVAC
from pysimmods.consumer.hvacsim.presets import (
    hvac_1279kw_init,
    hvac_1279kw_params,
)
from pysimmods.other.flexibility.flexibility_model import FlexibilityModel
from pysimmods.util.date_util import GER


class TestHVACFlexibility(unittest.TestCase):
    def setUp(self):
        self.start_date = "2018-05-19 14:00:00+0100"
        self.forecasts = pd.DataFrame(
            data=np.ones(28) * 21,
            columns=["t_air_deg_celsius"],
            index=pd.date_range(
                datetime.strptime(self.start_date, GER), periods=28, freq="15T"
            ),
        )

    @unittest.skip
    def test_generate_schedules(self):
        """Test the flexibility provision of the battery model."""
        flex = FlexibilityModel(HVAC(hvac_1279kw_params(), hvac_1279kw_init()))
        flex.update_forecasts(self.forecasts)

        flex.set_now_dt(self.start_date)
        flex.set_step_size(15 * 60)
        flex.inputs.t_air_deg_celsius = 28
        flex.step()

        flexis = flex.generate_schedules("2018-05-19 15:00:00+0100", 2, 10)

        self.assertIsInstance(flexis, dict)
        self.assertEqual(len(flexis), 10)

        for _, val in flexis.items():
            self.assertIsInstance(val, pd.DataFrame)
            self.assertEqual(val.columns[0], "target")
            self.assertEqual(val.columns[1], "p_kw")
            for target in val["target"].values:
                self.assertTrue(0 <= target < 100)
            for actual in val["p_kw"].values:
                self.assertTrue(0 <= actual <= 1279)


if __name__ == "__main__":
    unittest.main()
