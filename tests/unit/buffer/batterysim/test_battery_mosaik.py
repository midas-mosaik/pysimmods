import unittest

from pysimmods.buffer.batterysim.battery import Battery
from pysimmods.mosaik.pysim_mosaik import PysimmodsSimulator


class TestSimulator(unittest.TestCase):
    def setUp(self):
        self.sim = PysimmodsSimulator()
        self.sim.init(
            sid="TestSimulator-0",
            step_size=900,
            start_date="2018-05-19 14:00:00+0100",
        )
        self.bat_params = {
            "cap_kwh": 5,
            "soc_min_percent": 15,
            "p_charge_max_kw": 1,
            "p_discharge_max_kw": 1,
            "eta_pc": [-2.109566, 0.403556, 97.110770],
        }
        self.bat_inits = {"soc_percent": 50}
        self.inputs = {
            "Battery-0": {"p_set_mw": {"Test-0": 0.0005}},
            "Battery-1": {"p_set_mw": {"Test-0": 0}},
            "Battery-2": {"p_set_mw": {"Test-0": -0.00075}},
            # "Battery-3": {"set_percent": {"Test-0": 50.0}},
        }

    def test_create_bat(self):
        entities = self.sim.create(
            num=3,
            model="Battery",
            params=self.bat_params,
            inits=self.bat_inits,
        )

        self.assertEqual(len(entities), 3)
        for entity in entities:
            self.assertIsInstance(entity, dict)
            self.assertIsInstance(self.sim.models[entity["eid"]], Battery)

    def test_step(self):
        self.sim.create(
            num=4,
            model="Battery",
            params=self.bat_params,
            inits=self.bat_inits,
        )
        self.sim.step(0, self.inputs)

        self.assertEqual(0.5, self.sim.models["Battery-0"].get_p_kw())
        self.assertEqual(0.0, self.sim.models["Battery-1"].get_p_kw())
        self.assertEqual(-0.75, self.sim.models["Battery-2"].get_p_kw())
        # self.assertEqual(0.0, self.sim.models["Battery-3"].get_p_kw())

        self.assertAlmostEqual(
            52.4282507, self.sim.models["Battery-0"].state.soc_percent
        )
        self.assertAlmostEqual(
            50.0, self.sim.models["Battery-1"].state.soc_percent
        )
        self.assertAlmostEqual(
            46.1341311, self.sim.models["Battery-2"].state.soc_percent
        )

    def test_get_data(self):
        self.sim.create(
            num=3,
            model="Battery",
            params=self.bat_params,
            inits=self.bat_inits,
        )

        self.sim.step(0, self.inputs)
        outputs = {
            "Battery-0": ["p_mw", "soc_percent"],
            "Battery-1": ["p_mw", "soc_percent"],
            "Battery-2": ["p_mw", "soc_percent"],
        }

        data = self.sim.get_data(outputs)
        num_vals = 0

        self.assertIsInstance(data, dict)
        for key, value in data.items():
            self.assertIsInstance(key, str)
            self.assertIsInstance(value, dict)

            for attr, val in value.items():
                self.assertIsInstance(attr, str)
                self.assertIsInstance(val, float)
                num_vals += 1

        self.assertEqual(num_vals, 6)

        # From outside, the generator reference system in mw is used
        self.assertEqual(0.0005, data["Battery-0"]["p_mw"])
        self.assertEqual(0.0, data["Battery-1"]["p_mw"])
        self.assertEqual(-0.00075, data["Battery-2"]["p_mw"])


if __name__ == "__main__":
    unittest.main()
