import unittest

from pysimmods.generator import WindPowerPlant, WindPowerPlantSystem
from pysimmods.generator.windsystemsim.presets import wind_presets
from pysimmods.other.invertersim import Inverter


class TestWindTurbineSystem(unittest.TestCase):
    def test_init(self):
        wind_system = WindPowerPlantSystem(*wind_presets(2))

        self.assertIsInstance(wind_system.wind, WindPowerPlant)
        self.assertIsInstance(wind_system.inverter, Inverter)

    def test_step_p_set(self):
        wind_system = WindPowerPlantSystem(*wind_presets(2))

        wind_system.set_step_size(15 * 60)
        wind_system.set_now_dt("2021-06-08 14:00:00+0000")
        wind_system.inputs.t_air_deg_celsius = 8.2
        wind_system.inputs.air_pressure_hpa = 985.971
        wind_system.inputs.wind_v_m_per_s = 7.5

        wind_system.step()
        self.assertAlmostEqual(1.5426135, wind_system.get_p_kw())
        self.assertAlmostEqual(-0.7471218, wind_system.get_q_kvar())

        wind_system.set_step_size(15 * 60)
        wind_system.set_now_dt("2021-06-08 14:15:00+0000")
        wind_system.inputs.t_air_deg_celsius = 11.3
        wind_system.inputs.air_pressure_hpa = 987.971
        wind_system.inputs.wind_v_m_per_s = 9.2

        wind_system.set_p_kw(1.5)
        wind_system.step()

        self.assertEqual(1.5, wind_system.get_p_kw())
        self.assertAlmostEqual(-0.7264832, wind_system.get_q_kvar())


if __name__ == "__main__":
    unittest.main()
