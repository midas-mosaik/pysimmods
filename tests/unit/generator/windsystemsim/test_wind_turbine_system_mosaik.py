import sys
import unittest

from pysimmods.generator.windsystemsim.presets import wind_presets
from pysimmods.mosaik.pysim_mosaik import PysimmodsSimulator


class TestWindTurbineSysMosaik(unittest.TestCase):
    def setUp(self) -> None:
        self.sim = PysimmodsSimulator()
        self.sim.init(
            sid="PysimmodsSimulator-0",
            step_size=900,
            start_date="2021-06-08 14:00:00+0000",
        )
        self.inputs = {
            "WindTurbine-0": {
                "t_air_deg_celsius": {"DummySim-0.DummyMod-0": 23},
                "wind_v_m_per_s": {"DummySim-0.DummyMod-0": 1.0},
                "air_pressure_hpa": {"DummySim-0.DummyMod-0": 950.0},
            },
            "WindTurbine-1": {
                "t_air_deg_celsius": {"DummySim-0.DummyMod-0": 23},
                "wind_v_m_per_s": {"DummySim-0.DummyMod-0": 2.0},
                "air_pressure_hpa": {"DummySim-0.DummyMod-0": 950.0},
            },
            "WindTurbine-2": {
                "t_air_deg_celsius": {"DummySim-0.DummyMod-0": 13},
                "wind_v_m_per_s": {"DummySim-0.DummyMod-0": 12.5},
                "air_pressure_hpa": {"DummySim-0.DummyMod-0": 950.0},
            },
            "WindTurbine-3": {
                "t_air_deg_celsius": {"DummySim-0.DummyMod-0": 13},
                "wind_v_m_per_s": {"DummySim-0.DummyMod-0": 12.5},
                "air_pressure_hpa": {"DummySim-0.DummyMod-0": 950.0},
                "p_set_mw": {"DummySim-0.DummyMod-0": 0.0015},
            },
            "WindTurbine-4": {
                "t_air_deg_celsius": {"DummySim-0.DummyMod-0": 13},
                "wind_v_m_per_s": {"DummySim-0.DummyMod-0": 18.0},
                "air_pressure_hpa": {"DummySim-0.DummyMod-0": 950.0},
            },
            "WindTurbine-5": {
                "t_air_deg_celsius": {"DummySim-0.DummyMod-0": 13},
                "wind_v_m_per_s": {"DummySim-0.DummyMod-0": 20.0},
                "air_pressure_hpa": {"DummySim-0.DummyMod-0": 950.0},
            },
        }
        self.outputs = {
            "WindTurbine-0": ["p_mw", "p_kw", "q_mvar", "q_kvar"],
            "WindTurbine-1": ["p_mw", "p_kw", "q_mvar", "q_kvar"],
            "WindTurbine-2": ["p_mw", "p_kw", "q_mvar", "q_kvar"],
            "WindTurbine-3": ["p_mw", "p_kw", "q_mvar", "q_kvar"],
            "WindTurbine-4": ["p_mw", "p_kw", "q_mvar", "q_kvar"],
            "WindTurbine-5": ["p_mw", "p_kw", "q_mvar", "q_kvar"],
        }
        self.expected = {
            "WindTurbine-0": {
                "p_mw": 0.00000087,
                "p_kw": 0.00087033,
                "q_mvar": -0.00000042,
                "q_kvar": -0.00042152,
            },
            "WindTurbine-1": {
                "p_mw": 0.0000156917,
                "p_kw": 0.0156917226,
                "q_mvar": -0.00000756,
                "q_kvar": -0.00759985,
            },
            "WindTurbine-2": {
                "p_mw": 0.002,
                "p_kw": 2.0,
                "q_mvar": -0.00096864,
                "q_kvar": -0.96864420,
            },
            "WindTurbine-3": {
                "p_mw": 0.0015,
                "p_kw": 1.5,
                "q_mvar": -0.00072648,
                "q_kvar": -0.72648315,
            },
            "WindTurbine-4": {
                "p_mw": 0.002,
                "p_kw": 2.0,
                "q_mvar": -0.00096864,
                "q_kvar": -0.96864420,
            },
            "WindTurbine-5": {
                "p_mw": 0.0,
                "p_kw": 0.0,
                "q_mvar": -0.0,
                "q_kvar": -0.0,
            },
        }

    def test_get_data(self):
        """Test the usual functions of the mosaik simulator"""
        params, inits = wind_presets(2)

        self.sim.create(num=6, model="WindTurbine", params=params, inits=inits)

        self.sim.step(0, self.inputs)

        data = self.sim.get_data(self.outputs)

        for eid, actual in data.items():
            expected = self.expected[eid]
            for key in expected:
                self.assertAlmostEqual(
                    expected[key],
                    actual[key],
                    msg=f"Failed eid/key: {eid}/{key}",
                )


if __name__ == "__main__":
    unittest.main()
