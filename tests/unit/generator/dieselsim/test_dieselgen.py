import unittest

from pysimmods.generator.dieselsim import DieselGenerator
from pysimmods.generator.dieselsim.presets import diesel_presets


class TestDieselGenerator(unittest.TestCase):
    def setUp(self):
        self.params, self.inits = diesel_presets(10)
        self.params["sign_convention"] = "passive"

    def test_step(self):
        dieselgen = DieselGenerator(self.params, self.inits)

        dieselgen.set_now_dt("2021-02-21 00:00:00+0100")
        dieselgen.set_p_kw(-10)
        dieselgen.set_step_size(900)

        dieselgen.step()

        self.assertEqual(-10, dieselgen.get_p_kw())
        self.assertEqual(0, dieselgen.state.q_kvar)

    def test_default_schedule(self):
        dieselgen = DieselGenerator(self.params, self.inits)

        dieselgen.set_now_dt("2021-02-21 00:00:00+0100")
        dieselgen.set_step_size(900)

        dieselgen.step()

        self.assertEqual(6.0, dieselgen.config.default_p_schedule[0])
        self.assertEqual(-6, dieselgen.get_p_kw())

    def test_set_percent(self):
        self.params, self.inits = diesel_presets(50)
        self.params["sign_convention"] = "passive"
        dieselgen = DieselGenerator(self.params, self.inits)

        dieselgen.set_now_dt("2021-02-21 00:00:00+0100")
        dieselgen.set_percent(10)
        dieselgen.set_step_size(900)

        dieselgen.step()

        self.assertEqual(-9.5, dieselgen.get_p_kw())
        self.assertEqual(0, dieselgen.state.q_kvar)

    def test_sign_convention(self):
        self.params["sign_convention"] = "active"
        dieselgen = DieselGenerator(self.params, self.inits)

        dieselgen.set_now_dt("2017-01-01 00:00:00+0100")
        dieselgen.set_p_kw(7)
        dieselgen.set_step_size(900)

        dieselgen.step()

        self.assertEqual(7, dieselgen.get_p_kw())


if __name__ == "__main__":
    unittest.main()
