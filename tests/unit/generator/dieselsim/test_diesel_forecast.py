import unittest
from datetime import datetime, timedelta

import numpy as np
import pandas as pd

from pysimmods.generator.dieselsim import DieselGenerator
from pysimmods.generator.dieselsim.presets import diesel_presets
from pysimmods.other.flexibility.forecast_model import ForecastModel
from pysimmods.util.date_util import GER


class TestDieselForecast(unittest.TestCase):
    def setUp(self):
        self.start_date = "2021-02-21 00:00:00+0100"

    def test_step(self):
        params, inits = diesel_presets(50)
        params["sign_convention"] = "passive"
        diesel = DieselGenerator(params, inits)
        fcm = ForecastModel(DieselGenerator(params, inits))

        diesel.set_now_dt(self.start_date)
        diesel.set_step_size(15 * 60)
        diesel.set_p_kw(37.5)
        diesel.step()

        fcm.set_now_dt(self.start_date)
        fcm.set_step_size(15 * 60)
        fcm.set_p_kw(37.5)
        fcm.step()

        self.assertEqual(diesel.get_p_kw(), fcm.get_p_kw())

    def test_schedule(self):
        params, inits = diesel_presets(50)
        params["sign_convention"] = "passive"
        now_dt = datetime.strptime(self.start_date, GER)
        fcm = ForecastModel(
            DieselGenerator(params, inits), step_size=900, now_dt=now_dt
        )
        # TODO: Some values, need to be adapted
        expected = [0, 5, 10, 15, 20, 25, 30, 35, 40, 45, 50]

        schedule_df = pd.DataFrame(
            data=np.arange(0, 55, step=5),
            columns=["p_set_kw"],
            index=pd.date_range(now_dt, periods=11, freq="15min"),
        )
        fcm.update_schedule(schedule_df)

        for setpoint in expected:
            fcm.set_now_dt(now_dt)
            fcm.set_step_size(15 * 60)
            fcm.step()
            now_dt += timedelta(seconds=900)

            self.assertEqual(-setpoint, fcm.get_p_kw())


if __name__ == "__main__":
    unittest.main()
