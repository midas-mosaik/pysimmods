import unittest

from pysimmods.generator.windsim.presets import wind_power_plant_preset
from pysimmods.generator.windsim.wind import WindPowerPlant


class TestWindPowerPlant(unittest.TestCase):
    def test_step(self):
        turbine = WindPowerPlant(
            *wind_power_plant_preset(pn_max_kw=8, turbine_type="SCD168/8000")
        )

        turbine.inputs.t_air_deg_celsius = 8.2
        turbine.inputs.air_pressure_hpa = 985.971
        turbine.inputs.wind_v_m_per_s = 7.5
        turbine.set_step_size(900)

        turbine.step()

        self.assertAlmostEqual(5.42029, turbine.get_p_kw(), places=5)
        self.assertAlmostEqual(
            10.4941, turbine.state.wind_hub_v_m_per_s, places=4
        )
        self.assertAlmostEqual(
            280.732, turbine.state.t_air_hub_deg_kelvin, places=3
        )
        self.assertAlmostEqual(
            1.20878, turbine.state.air_density_hub_kg_per_m3, places=5
        )

    def test_wind_profile(self):
        turbine_hellmann = WindPowerPlant(
            *wind_power_plant_preset(
                pn_max_kw=8,
                wind_profile="hellmann",
                turbine_type="SCD168/8000",
            )
        )
        turbine_log = WindPowerPlant(
            *wind_power_plant_preset(
                pn_max_kw=8,
                wind_profile="logarithmic",
                turbine_type="SCD168/8000",
            )
        )

        self.assertEqual(turbine_hellmann.config.wind_profile, "hellmann")
        self.assertEqual(turbine_log.config.wind_profile, "logarithmic")

        for turbine in [turbine_hellmann, turbine_log]:
            turbine.inputs.t_air_deg_celsius = 8.2
            turbine.inputs.air_pressure_hpa = 985.971
            turbine.inputs.wind_v_m_per_s = 7.5
            turbine.set_step_size(900)

            turbine.step()

        # Calculated wind speeds differ
        self.assertAlmostEqual(
            10.4941, turbine_hellmann.state.wind_hub_v_m_per_s, places=4
        )
        self.assertAlmostEqual(
            11.6992, turbine_log.state.wind_hub_v_m_per_s, places=4
        )

        # Therefore, power output differs as well
        self.assertAlmostEqual(5.42029, turbine_hellmann.get_p_kw(), places=5)
        self.assertAlmostEqual(6.90704, turbine_log.get_p_kw(), places=5)

    def test_temperature_profile(self):
        turbine_linear = WindPowerPlant(
            *wind_power_plant_preset(
                pn_max_kw=8,
                temperature_profile="linear_gradient",
                turbine_type="SCD168/8000",
            )
        )
        turbine_np = WindPowerPlant(
            *wind_power_plant_preset(
                pn_max_kw=8,
                temperature_profile="no_profile",
                turbine_type="SCD168/8000",
            )
        )

        self.assertEqual(
            turbine_linear.config.temperature_profile, "linear_gradient"
        )
        self.assertEqual(turbine_np.config.temperature_profile, "no_profile")

        for turbine in [turbine_linear, turbine_np]:
            turbine.inputs.t_air_deg_celsius = 8.2
            turbine.inputs.air_pressure_hpa = 985.971
            turbine.inputs.wind_v_m_per_s = 7.5
            turbine.set_step_size(900)

            turbine.step()

        # Calculated temperatures differ
        self.assertAlmostEqual(
            280.732, turbine_linear.state.t_air_hub_deg_kelvin, places=3
        )
        self.assertAlmostEqual(
            281.35, turbine_np.state.t_air_hub_deg_kelvin, places=3
        )

        # Therefore, power output differs as well
        self.assertAlmostEqual(5.42029, turbine_linear.get_p_kw(), places=5)
        self.assertAlmostEqual(5.40830, turbine_np.get_p_kw(), places=5)

    def test_air_density_correction(self):
        turbine_barometric = WindPowerPlant(
            *wind_power_plant_preset(
                pn_max_kw=8,
                air_density_profile="barometric",
                turbine_type="SCD168/8000",
            )
        )
        turbine_ideal_gas = WindPowerPlant(
            *wind_power_plant_preset(
                pn_max_kw=8,
                air_density_profile="ideal_gas",
                turbine_type="SCD168/8000",
            )
        )
        turbine_np = WindPowerPlant(
            *wind_power_plant_preset(
                pn_max_kw=8,
                air_density_profile="no_profile",
                turbine_type="SCD168/8000",
            )
        )

        for turbine in [turbine_barometric, turbine_ideal_gas, turbine_np]:
            turbine.inputs.t_air_deg_celsius = 8.2
            turbine.inputs.air_pressure_hpa = 985.971
            turbine.inputs.wind_v_m_per_s = 7.5
            turbine.set_step_size(900)

            turbine.step()

        self.assertAlmostEqual(
            1.20878,
            turbine_barometric.state.air_density_hub_kg_per_m3,
            places=5,
        )
        self.assertAlmostEqual(
            1.20876,
            turbine_ideal_gas.state.air_density_hub_kg_per_m3,
            places=5,
        )
        self.assertAlmostEqual(
            1.225, turbine_np.state.air_density_hub_kg_per_m3, places=5
        )

        self.assertAlmostEqual(
            5.42029, turbine_barometric.get_p_kw(), places=5
        )
        self.assertAlmostEqual(5.42019, turbine_ideal_gas.get_p_kw(), places=5)
        self.assertAlmostEqual(5.49411, turbine_np.get_p_kw(), places=5)


if __name__ == "__main__":
    unittest.main()
