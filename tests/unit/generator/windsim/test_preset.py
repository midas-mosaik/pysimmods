import unittest

from pysimmods.generator.windsim.presets import wind_power_plant_preset


class Testpreset(unittest.TestCase):
    # def setUp(self):
    #     self.pn_max_kw = (
    #         3  # the  nominal power value to select the turbine configuration
    #     )

    # #   #  dictt = { "method","power_cofficient"}
    # #     pn_max_kw =  3
    # #     self.allpreset =  wind_power_plant_preset(pn_max_kw, method= 
    # "power_cofficient")
    # #     self.data_pres= self.allpreset[0]
    # #     self.data = self.allpreset[0]
    # #     self.params, self.inits = wind_power_plant_preset(pn_max_kw)
    # #     self.turbine = WindPowerPlant(self.params,self.inits)
    # #     self.config = TurbineConfig(self.params)

    def test_preset(self):  #  check preset values
        expected = [
            (2, "E-82/2000"),
            (3, "E-82/3000"),
            (5, "AD116/5000"),
            (8, "V164/8000"),
        ]
        for pn_max_kw, ttype in expected:
            params, inits = wind_power_plant_preset(pn_max_kw=pn_max_kw)

            self.assertEqual(ttype, params["turbine_type"])
            self.assertEqual(pn_max_kw, params["pn_max_kw"])

    def test_preset_any(self):  #  check preset values
        expected = [
            (0.5, "E-82/2000"),
            (2.3, "E-82/2000"),
            (2.5, "E-82/3000"),
            (10, "V164/8000"),
        ]
        for pn_max_kw, ttype in expected:
            params, inits = wind_power_plant_preset(pn_max_kw=pn_max_kw)

            self.assertEqual(ttype, params["turbine_type"])
            self.assertEqual(pn_max_kw, params["pn_max_kw"])


if __name__ == "__main__":
    unittest.main()
