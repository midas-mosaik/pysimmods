import os
import unittest
from datetime import datetime, timedelta

import pandas as pd
from midas.util.dateformat import GER

from pysimmods.generator.pvsim import PhotovoltaicPowerPlant
from pysimmods.generator.pvsystemsim import PVPlantSystem
from pysimmods.generator.pvsystemsim.presets import pv_preset
from pysimmods.other.invertersim import Inverter


class TestQSetpoints(unittest.TestCase):
    def test_setpoints(self):
        pvsys = PVPlantSystem(*pv_preset(700))

        wdata = pd.read_csv(
            os.path.abspath(
                os.path.join(
                    __file__,
                    "..",
                    "..",
                    "..",
                    "..",
                    "fixtures",
                    "weather-time-series.csv",
                )
            )
        )
        p_mws = []
        q_mvars = []
        now_dt = datetime.strptime("2020-07-01 00:00:00+0000", GER)
        for idx in range(17588, 17664):
            row = wdata.iloc[idx]

            pvsys.set_now_dt(now_dt)
            pvsys.set_step_size(900)
            pvsys.inputs.bh_w_per_m2 = row["WeatherCurrent__0___bh_w_per_m2"]
            pvsys.inputs.dh_w_per_m2 = row["WeatherCurrent__0___dh_w_per_m2"]
            pvsys.inputs.t_air_deg_celsius = row[
                "WeatherCurrent__0___t_air_deg_celsius"
            ]
            pvsys.set_q_kvar(-777)

            pvsys.step()

            self.assertLess(pvsys.get_q_kvar(), 0)
            p_mws.append(pvsys.get_p_kw())
            q_mvars.append(pvsys.get_q_kvar())
            now_dt += timedelta(seconds=900)

        # print(p_mws)

        # print(q_mvars)


if __name__ == "__main__":
    unittest.main()
