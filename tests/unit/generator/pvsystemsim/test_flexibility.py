"""This module contains the tests for the flexibility model."""

import unittest
from datetime import datetime, timedelta

import numpy as np
import pandas as pd

from pysimmods.generator.pvsystemsim.pvpsystem import PVPlantSystem
from pysimmods.other.flexibility.flexibility_model import FlexibilityModel
from pysimmods.util.date_util import GER


class TestFlexibilityModel(unittest.TestCase):
    def setUp(self):
        self.params = {
            "pv": {
                "lat_deg": 48.40,
                "lon_deg": 9.98,
                "orient_deg": 180,
                "tilt_deg": 30,
                "eta_percent": 15,
                "beta_percent_per_kelvin": 0.29,
                "k_m_w_per_m2": 50,
                "a_m2": 15,
                "alpha_w_per_m2_kelvin": 11,
                "rho_kg_per_m2": 15,
                "reflex_percent": 5,
                "c_j_per_kg_kelvin": 900,
            },
            "inverter": {
                "sn_kva": 4,
                "q_control": "p_set",
                "cos_phi": 0.9,
                "inverter_mode": "capacitive",
            },
        }
        self.inits = {"pv": {"t_module_deg_celsius": 25}, "inverter": None}
        self.start_date = "2006-08-01 12:00:00+0100"
        bhs = [
            505,
            510,
            520,
            530,
            540,
            550,
            560,
            570,
            580,
            590,
            600,
            610,
            600,
            590,
            580,
            570,
            560,
            550,
            540,
            530,
            520,
            510,
            500,
            490,
            500,
            510,
            520,
            530,
            540,
            550,
        ]
        dhs = [
            305,
            310,
            320,
            330,
            340,
            350,
            360,
            370,
            380,
            390,
            400,
            410,
            410,
            400,
            390,
            380,
            370,
            360,
            350,
            340,
            330,
            320,
            310,
            300,
            300,
            310,
            320,
            330,
            340,
            350,
        ]
        t_airs = [
            20.2,
            20.5,
            21,
            21.5,
            22,
            22.5,
            23,
            23,
            23,
            23,
            23,
            23,
            22.5,
            22.5,
            22.5,
            22.5,
            22.5,
            22.5,
            22,
            21.5,
            21,
            20.5,
            20,
            19.5,
            19,
            19,
            19,
            19,
            19,
            19,
        ]
        index = pd.date_range(
            datetime.strptime(self.start_date, GER),
            datetime.strptime(self.start_date, GER)
            + timedelta(hours=2, minutes=25),
            periods=30,
        )
        self.forecasts = pd.DataFrame(
            data=np.array([bhs, dhs, t_airs]).transpose(),
            columns=["bh_w_per_m2", "dh_w_per_m2", "t_air_deg_celsius"],
            index=index,
        )

    @unittest.skip
    def test_generate_schedules(self):
        """Test the generate schedules functionality."""

        flex = FlexibilityModel(PVPlantSystem(self.params, self.inits))
        flex.set_now_dt(self.start_date)
        flex.set_step_size(5 * 60)
        flex.inputs.bh_w_per_m2 = 500
        flex.inputs.dh_w_per_m2 = 300
        flex.inputs.t_air_deg_celsius = 20

        flex.step()

        flex.update_forecasts(self.forecasts)
        # flex.update_schedule(self.schedule)

        flexis = flex.generate_schedules("2006-08-01 12:10:00+0100", 2, 10)

        self.assertIsInstance(flexis, dict)
        self.assertEqual(len(flexis), 10)

        for _, val in flexis.items():
            self.assertIsInstance(val, pd.DataFrame)
            self.assertEqual(val.columns[0], "target")
            self.assertEqual(val.columns[1], "p_kw")
            for target in val["target"].values:
                self.assertTrue(0 <= target < 100)
            for actual in val["p_kw"].values:
                self.assertTrue(-2.5 <= actual <= 0)


if __name__ == "__main__":
    unittest.main()
