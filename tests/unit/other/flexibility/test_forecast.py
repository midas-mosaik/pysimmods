import unittest
from datetime import datetime, timedelta
from unittest.mock import MagicMock

import numpy as np
import pandas as pd

from pysimmods.other.dummy.model import DummyModel
from pysimmods.other.flexibility.forecast_model import ForecastModel
from pysimmods.util.date_util import GER


class TestForecastModel(unittest.TestCase):
    def setUp(self):
        self.start_date = "2021-12-14 13:00:00+0000"
        self.step_size = 900
        self.model = DummyModel({}, {})

    def test_check_schedule(self):
        """Test the internal method check_schedule.

        If a new schedule is required, the _create_init_schedule
        method will be called.

        """
        flex = ForecastModel(
            DummyModel(dict(), dict()), forecast_horizon_hours=1
        )

        flex.schedule = MagicMock()
        flex.schedule.get = MagicMock(return_value=25)
        flex.schedule.reschedule_required = MagicMock(return_value=False)
        flex._create_default_schedule = MagicMock()
        flex._now_dt = datetime.strptime(self.start_date, GER)
        flex._step_size = self.step_size
        flex._percent_factor = 1.0

        flex._check_schedule()

        flex.schedule.reschedule_required.assert_called_once()
        flex._create_default_schedule.assert_not_called()
        flex.schedule.prune.assert_not_called()

        flex.schedule.reschedule_required = MagicMock(return_value=True)

        flex._check_schedule()

        flex.schedule.reschedule_required.assert_called_once()
        flex._create_default_schedule.assert_called_once()
        flex.schedule.prune.assert_called_once()

    def test_calculate_step(self):
        """Test the internal method perform_step."""

        flex = ForecastModel(
            DummyModel(dict(), dict()), forecast_horizon_hours=1
        )
        flex.set_now_dt(self.start_date)
        flex.set_step_size(self.step_size)
        flex._check_inputs()

        now = flex._now_dt + timedelta(seconds=self.step_size)

        flex._calculate_step(now, 234.5, 67.8)
        self.assertEqual(flex._model.get_p_kw(), 234.5)
        self.assertEqual(flex._model.get_q_kvar(), 67.8)

    def test_create_default_schedule(self):
        """Test the interal method create_default_schedule."""
        flex = ForecastModel(
            DummyModel(dict(), dict()), forecast_horizon_hours=1
        )
        flex.set_now_dt(self.start_date)
        flex.set_step_size(self.step_size)
        flex._check_inputs()

        flex._model.get_state()

        flex._create_default_schedule()

        rows = int(flex._horizon_hours * 1 * 4) + 1

        self.assertEqual(len(flex.schedule._data.index), rows)

    def test_step(self):
        """Test the regular step of the underlying model.

        Using only the step method, the results should be the same.

        """
        mod = DummyModel(dict(), dict())
        flex = ForecastModel(
            DummyModel(dict(), dict()), forecast_horizon_hours=1
        )

        mod.set_now_dt(self.start_date)
        mod.set_step_size(self.step_size)
        mod.set_p_kw(234.5)
        mod.step()

        self.assertEqual(mod.get_p_kw(), 234.5)

        flex.set_now_dt(self.start_date)
        flex.set_step_size(self.step_size)
        flex.set_p_kw(234.5)
        flex.step()

        self.assertEqual(flex.get_p_kw(), 234.5)

    def test_update_forecast(self):
        """Test the method update_forecasts."""
        t_airs = (
            [20.2, 20.5, 21, 21.5, 22, 22.5, 23, 23]
            + [23, 23, 23, 23, 22.5, 22.5, 22.5, 22.5]
            + [22.5, 22.5, 22, 21.5, 21, 20.5, 20, 19.5]
            + [19, 19, 19, 19, 19, 19]
        )
        forecast_1 = pd.DataFrame(
            data=np.array([t_airs[:6]]).reshape(-1, 1),
            columns=["t_air_deg_celsius"],
            index=pd.date_range(
                datetime.strptime(self.start_date, GER),
                periods=6,
                freq="15min",
            ),
        )
        forecast_2 = pd.DataFrame(
            data=np.array([t_airs[4:10]]).reshape(-1, 1),
            columns=["t_air_deg_celsius"],
            index=pd.date_range(
                datetime.strptime(self.start_date, GER)
                + timedelta(seconds=900 * 4),
                periods=6,
                freq="15min",
            ),
        )

        forecast_3 = pd.DataFrame(
            data=np.array([t_airs]).reshape(-1, 1),
            columns=["t_air_deg_celsius"],
            index=pd.date_range(
                datetime.strptime(self.start_date, GER),
                periods=30,
                freq="15min",
            ),
        )

        bhs = (
            [505, 510, 520, 530, 540, 550]
            + [560, 570, 580, 590, 600, 610]
            + [600, 590, 580, 570, 560, 550]
            + [540, 530, 520, 510, 500, 490]
            + [500, 510, 520, 530, 540, 550]
        )

        forecast_4 = pd.DataFrame(
            data=np.array([bhs]).reshape(-1, 1),
            columns=["bh_w_per_m2"],
            index=pd.date_range(
                datetime.strptime(self.start_date, GER),
                periods=30,
                freq="15min",
            ),
        )

        flex = ForecastModel(
            DummyModel(dict(), dict()), forecast_horizon_hours=1
        )
        flex.update_forecasts(forecast_1)
        self.assertEqual(flex._forecasts.size, 6)

        flex.update_forecasts(forecast_2)
        self.assertEqual(flex._forecasts.size, 10)

        flex.update_forecasts(forecast_3)
        self.assertEqual(flex._forecasts.size, 30)

        flex.update_forecasts(forecast_4)
        self.assertEqual(flex._forecasts.size, 60)

    def test_unit_factor(self):
        """Test the usage of different unit factors."""

        flex_kw = ForecastModel(
            DummyModel(dict(), dict()), forecast_horizon_hours=1
        )

        flex_mw = ForecastModel(
            DummyModel(dict(), dict()), unit="mw", forecast_horizon_hours=1
        )
        now_dt = datetime.strptime(self.start_date, GER)
        # P is always set in kw, the difference is only visible in the
        # schedule.
        flex_kw.set_now_dt(now_dt)
        flex_kw.set_step_size(self.step_size)
        flex_mw.set_now_dt(now_dt)
        flex_mw.set_step_size(self.step_size)

        flex_kw.set_p_kw(234.5)
        flex_mw.set_p_kw(234.5)

        flex_kw.step()
        flex_mw.step()

        self.assertEqual(
            234.5, flex_kw.schedule.get(self.start_date, "p_set_kw")
        )
        self.assertAlmostEqual(
            0.2345, flex_mw.schedule.get(self.start_date, "p_set_mw")
        )

        # Now we provide a schedule for the next three timesteps
        now = flex_kw.schedule._data.index[1]
        next1 = flex_kw.schedule._data.index[2]
        next2 = flex_kw.schedule._data.index[3]

        flex_kw.update_schedule(
            pd.DataFrame({"p_set_kw": [25, 75, 30]}, index=[now, next1, next2])
        )
        flex_mw.update_schedule(
            pd.DataFrame(
                {"p_set_mw": [0.025, 0.075, 0.03]}, index=[now, next1, next2]
            )
        )

        now_dt += timedelta(seconds=self.step_size)
        flex_kw.set_now_dt(now_dt)
        flex_kw.set_step_size(self.step_size)
        flex_mw.set_now_dt(now_dt)
        flex_mw.set_step_size(self.step_size)
        # The models should follow the schedule
        flex_kw.step()
        flex_mw.step()

        self.assertEqual(25, flex_kw.schedule.get(now_dt, "p_set_kw"))
        self.assertEqual(0.025, flex_mw.schedule.get(now_dt, "p_set_mw"))

        now_dt += timedelta(seconds=self.step_size)
        flex_kw.set_now_dt(now_dt)
        flex_kw.set_step_size(self.step_size)
        flex_mw.set_now_dt(now_dt)
        flex_mw.set_step_size(self.step_size)
        # Without further input, the model will now use the next value
        # of the schedule
        flex_kw.step()
        flex_mw.step()

        self.assertEqual(75, flex_kw.schedule.get(now_dt, "p_set_kw"))
        self.assertEqual(0.075, flex_mw.schedule.get(now_dt, "p_set_mw"))

        now_dt += timedelta(seconds=self.step_size)
        flex_kw.set_now_dt(now_dt)
        flex_kw.set_step_size(self.step_size)
        flex_mw.set_now_dt(now_dt)
        flex_mw.set_step_size(self.step_size)
        # Now we give a direct input again. Since the models priorize
        # schedule values, the direct input will be ignored.
        flex_kw.set_p_kw(200)
        flex_mw.set_p_kw(200)

        flex_kw.step()
        flex_mw.step()

        self.assertEqual(30, flex_kw.schedule.get(now_dt, "p_set_kw"))
        self.assertEqual(0.03, flex_mw.schedule.get(now_dt, "p_set_mw"))


if __name__ == "__main__":
    unittest.main()
