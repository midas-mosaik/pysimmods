Overview
========

Pysimmods consist of four generation models (Biogas plant, combined heat and power, Photovoltaic, and Diesel generator), one buffer model (electrical battery), and one consumer (heat ventilation and air conditioning unit).
More models will probably be included in the future.
There are also some utility models, including a DC/AC inverter, a heat storage and a heat demand model.
Furthermore, there are two model *systems*, which combine one of the base models and one or more utility models.

The API of the models is streamlined, i.e., the usage is mostly similar except for some model-specific attributes.
Because of this, there also exist three model wrapper that increase the model-specific functionality by model-agnostic features.
The first one is the *ScheduleModel*, which enables the models to receive and follow a certain schedule.
The second one is the *ForecastModel*, which extends the *ScheduleModel* in the sense that the model is able to predict the outcome of the *n* future steps.
Finally, there is the *FlexibilityModel*, which is built on top of the *ForecastModel* and which allows to create an arbitrary number of possible schedules in a future time frame.
