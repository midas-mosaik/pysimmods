Base Models
===========

.. toctree::
   :maxdepth: 2

   battery
   hvac
   biogas
   chp
   pv
   diesel
