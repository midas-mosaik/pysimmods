Model Wrapper
=============

The main purpose of model wrappers are to add functionality to the *pysimmods* models that

   a. has nothing to do with the original purpose of the model and
   b. can be used by every model and therefore would otherwise require to be implemented several times.

.. toctree::
   :maxdepth: 2

   schedule
   forecast
   flexibility
