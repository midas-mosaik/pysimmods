.. pysimmods documentation master file, created by
   sphinx-quickstart on Thu Sep 10 14:46:51 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to pysimmods's documentation!
=====================================

This projects contains a collection of (python) simulation models for a variety of energy generation and consumption units.
Most of the models were originally developed in the context of the research project iHEM (intelligent Home Energy Management) or other projects at OFFIS.
Other models were developed during a student project group at the University of Oldenburg. 
The models are intended to be used with the co-simulation framework mosaik (https://mosaik.offis.de), but are also usable without mosaik.

.. toctree::
   :maxdepth: 2

   overview
   installation
   base-models/index
   util-models/index
   wrapper/index
   mosaik/index


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
